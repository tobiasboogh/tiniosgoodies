//
//  TINViewController.h
//  TINiOSGoodies
//
//  Created by Tobias Boogh on 11/05/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TINGradientBackground.h"
@interface TINViewController : UIViewController
@property (weak, nonatomic) IBOutlet TINGradientBackground *gradientBackground;

@end
